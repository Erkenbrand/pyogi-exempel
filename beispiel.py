#!/usr/bin/env python3

"""pyogi Test und Beispiel."""

import sys

import pygame
from pygame.freetype import SysFont

import pyogi
from pyogi.locals import *  # pygame.locals werden mit importiert.


# init der pygame Umgebung.
pygame.init()
pygame.key.set_repeat(1, 1)  # sollte immer gesetzt werden.

RESULUTION = (600, 600)
display = pygame.display.set_mode(RESULUTION)

CLOCK = pygame.time.Clock()
FPS = 30

# Erstelle ein Standard Font Objekt fuer die Gui.
# Es muss ein freetype Font sein
DEFAULTFONT = SysFont(None, 12)
DEFAULTFONT.pad = True

DEFAULTFONTVERTIVAL = SysFont(None, 12)
DEFAULTFONTVERTIVAL.vertical = True
DEFAULTFONTVERTIVAL.pad = True

# Das Haupt Fenster, der Gui mit der Groeße des ganzen Fensters.
gui = pyogi.Gui(RESULUTION)

# Erstellung aller anderen Gui Elemente.
container = pyogi.Container(size=(160, 160))
text = pyogi.Text("Test", font=DEFAULTFONT, text_bg=(255, 255, 255))
entry = pyogi.Entry(font=DEFAULTFONT)
entry_text = pyogi.Text("Entry", font=DEFAULTFONT, text_fg=(255, 255, 255),
                        text_bg=(100, 100, 155))
mehrzeiligertext = pyogi.Multiline(
    "das ist zeile 1\nund das ist zeile 2\n______", DEFAULTFONT,
    text_fg=(255, 255, 255), from_side="midtop", to_side="midbottom")
liste = [pyogi.Text("das ist text" + str(num), DEFAULTFONT,
                    text_fg=(255, 255, 0)) for num in range(0, 6)]

text_exit = pyogi.Text("EXIT", font=DEFAULTFONT, text_fg=(255, 0, 0),
                       text_bg=(255, 255, 255))
rot = pyogi.BaseColor((40, 40), (255, 0, 0))
gruen = pyogi.BaseColor((40, 40), (0, 255, 0))
entry2 = pyogi.Entry(font=DEFAULTFONT, widget_bg=(255, 255, 255))
entry_text2 = pyogi.Text("Entry", font=DEFAULTFONT, text_fg=(255, 255, 255),
                         text_bg=(100, 100, 155))

info = pyogi.Text("", font=DEFAULTFONT, text_bg=(255, 255, 255, 160))


def set_popup(text="", text_bg=(100, 100, 155, 155), text_fg=(255, 255, 255)):
    pyogi.popup.set(info, pygame.mouse.get_pos(), "midbottom")
    info.text = text
    info.config(text_bg=text_bg, text_fg=text_fg)


# einstellen der event eingaben.
text_exit.bind_event_mode(MOUSEBUTTONUP, 1, sys.exit)
text_exit.bind_event(MOUSE_GO_IN,
                     lambda: text_exit.config(text_bg=(100, 100, 155)))
text_exit.bind_event(MOUSE_GO_OUT,
                     lambda: text_exit.config(text_bg=(255, 255, 255)))
rot.bind_event(MOUSE_GO_IN, lambda: set_popup("lol rot", (0, 255, 0, 155)))
gruen.bind_event(MOUSE_GO_IN, lambda: set_popup("lol gruen", (255, 0, 0, 155)))
rot.bind_event(MOUSEMOTION, lambda: pyogi.popup.move(pygame.mouse.get_pos()))
gruen.bind_event(MOUSEMOTION, lambda: pyogi.popup.move(pygame.mouse.get_pos()))
rot.bind_event(MOUSE_GO_OUT, pyogi.popup.clean)
gruen.bind_event(MOUSE_GO_OUT, pyogi.popup.clean)

# setzt den Fokus der Gui, auf das Widget wen es angeklickt wird.
container.bind_event_mode(MOUSEBUTTONUP, 1, container.focussing)

# Ruft eine Funktion auf bei Fokussierung.
container.bind_event(SET_FOCUS, lambda: print("focus set at container"))
container.bind_event(LOST_FOCUS, lambda: print("container lost focus"))

# platziert alle Widget die, ins Gui sollen.
gui.place(container, side="center")
gui.place(text, side="bottomright")
gui.place(entry, side="topright")

# platziert alle Widget die, in den Container sollen.
container.place(rot)
container.place(gruen, side="bottomright")
container.place(text_exit, side="center")
container.place(entry2, side="midbottom")

# platziert anliegend an ein anderes Widget.
pyogi.place.to(container, mehrzeiligertext, offset=(0, 0))
pyogi.place.to(entry, entry_text, side="midright", offset=(-2, 0))
pyogi.place.to(entry2, entry_text2, side="midright", offset=(-2, 0))

# platziert die Widget, in der liste in einer reihe.
pyogi.place.in_row(gui, liste, (0, 0), offset=(0, 10))

if __name__ == '__main__':

    while True:
        for event in pygame.event.get():
            gui.test_event(event)
            etype = event.type
            if etype == QUIT:
                sys.exit()
            elif etype == VIDEORESIZE:
                pass

        # muss immer aufgerufen werden
        gui.update()
        if gui.changed:  # prueft auf eine AEnderung in der Gui
            display.fill((0, 0, 0))
            gui.draw(display)  # Zeichnet die Gui auf display

        past_time = CLOCK.tick(FPS)
        pygame.display.flip()
