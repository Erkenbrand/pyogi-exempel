#!/usr/bin/env python3

"""pyogi Test und Beispiel"""

import sys

import pygame
from pygame.freetype import Font, SysFont

import pyogi
from pyogi.locals import * # pygame.locals werden mit importiert
import pyogi.theme.dark as dark_theme
import pyogi.theme.crazy as crazy_theme

#init der pygame Umgebung
pygame.init()
pygame.key.set_repeat(1, 1) #sollte immer gesetzt werden

CLOCK = pygame.time.Clock()
FPS = 30
RESULUTION = (600, 600)
display = pygame.display.set_mode(RESULUTION)

#Erstelle ein Standard Font Objekt fuer die Gui
#Es muss ein freetype Font sein
DEFAULTFONT = SysFont(None, 12)
DEFAULTFONT.pad = True

DEFAULTFONTVERTIVAL = SysFont(None, 12)
DEFAULTFONTVERTIVAL.vertical = True
DEFAULTFONTVERTIVAL.pad = True

#Das Haupt Fenster der Gui mit der Groeße des ganzen Fensters
gui = pyogi.Gui(RESULUTION)

#Erstellung aller anderen Gui Elemente
info = pyogi.Text("DropdownList", DEFAULTFONT,text_bg=(255,255,255))
dropdownlist = pyogi.Dropdownlist(DEFAULTFONT)

dropdownlist.add_rows("eintrag 1", "eintrag 2", "eintrag 3", "eintrag 4")

#einstellen der event eingaben
def set_popup():
    pyogi.popup.set(info, pygame.mouse.get_pos(), "midbottom")

dropdownlist.bind_event(MOUSE_GO_IN, set_popup)
dropdownlist.bind_event(MOUSE_GO_OUT, pyogi.popup.clean)
dropdownlist.bind_event(MOUSEMOTION,
                        lambda : pyogi.popup.move(pygame.mouse.get_pos()))

#platzierung aller Widget.
gui.place(dropdownlist, (0,0), "center", "center")

if __name__ == '__main__':

    while True:
        for event in pygame.event.get():
            gui.test_event(event)
            etype = event.type
            if etype == QUIT:
                sys.exit()
            elif etype == VIDEORESIZE:
                pass

        #muss immer aufgerufen werden
        gui.update()
        if gui.changed: #prueft auf eine AEnderung in der Gui
            display.fill((0, 0, 0))
            gui.draw(display) #Zeichnet die Gui auf display

        past_time = CLOCK.tick(FPS)
        pygame.display.flip()
