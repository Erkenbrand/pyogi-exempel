#!/usr/bin/env python3

"""pyogi Test und Beispiel"""

import sys

import pygame.rect
from pygame.freetype import SysFont
import pyogi
from pyogi.locals import *  # pygame.locals werden mit importiert.

# init der pygame Umgebung.
pygame.init()
pygame.key.set_repeat(1, 1)  # sollte immer gesetzt werden.

RESULUTION = (600, 600)
display = pygame.display.set_mode(RESULUTION, RESIZABLE)

CLOCK = pygame.time.Clock()
FPS = 30

# Erstelle ein Standard Font Objekt fuer die Gui.
# Es muss ein freetype Font sein
DEFAULTFONT = SysFont(None, 12)
DEFAULTFONT.pad = True

DEFAULTFONTVERTIVAL = SysFont(None, 12)
DEFAULTFONTVERTIVAL.vertical = True
DEFAULTFONTVERTIVAL.pad = True

# Das Haupt Fenster, der Gui mit der Groeße des ganzen Fensters.
gui = pyogi.Gui(size=(RESULUTION))
ic1 = pyogi.Button(label=pyogi.Text("test1", font=DEFAULTFONT))
ic2 = pyogi.Button(label=pyogi.Text("test2", font=DEFAULTFONT))
ic3 = pyogi.Button(label=pyogi.Text("test3", font=DEFAULTFONT))

gui.add_all((ic2, ic3))

gui.place(ic1, side="center", link=True)
ic2.link_side(ic1, side="topright")



if __name__ == '__main__':

    while True:
        for event in pygame.event.get():
            gui.test_event(event)
            etype = event.type
            if etype == QUIT:
                sys.exit()
            elif etype == VIDEORESIZE:
                display = pygame.display.set_mode(event.size, RESIZABLE)

        # muss immer aufgerufen werden
        gui.update()
        if gui.changed:  # prueft auf eine AEnderung in der Gui
            display.fill((255, 255, 255))
            gui.draw(display)  # Zeichnet die Gui auf display

        past_time = CLOCK.tick(FPS)
        pygame.display.flip()
