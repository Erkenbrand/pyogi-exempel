#!/usr/bin/env python3

"""pyogi Test und Beispiel"""

import sys

import pygame
from pygame.freetype import SysFont

import pyogi
from pyogi.locals import * # pygame.locals werden mit importiert.
import pyogi.theme.dark as dark_theme
import pyogi.theme.crazy as crazy_theme

themes = {"Dark":dark_theme, "Crazy":crazy_theme, "Normal":DEFAULT_THEME,
          "Keines":None}

#init der pygame Umgebung.
pygame.init()
pygame.key.set_repeat(1, 1) #sollte immer gesetzt werden.

CLOCK = pygame.time.Clock()
FPS = 30
RESULUTION = (600, 600)
display = pygame.display.set_mode(RESULUTION)

#Erstelle ein Standard Font Objekt fuer die Gui.
#Es muss ein freetype Font sein.
DEFAULTFONT = SysFont(None, 12)

DEFAULTFONTVERTIVAL = SysFont(None, 12)
DEFAULTFONTVERTIVAL.vertical = True
DEFAULTFONTVERTIVAL.pad = True

#Das Haupt Fenster, der Gui mit der Groeße des ganzen Fensters.
gui = pyogi.Gui(RESULUTION)

#Erstellung aller anderen Gui Elemente.

config = pyogi.Frame(pyogi.Container((230, 42)))
themen_wahl = pyogi.Label("themen wahl", DEFAULTFONT,
                                  size=(120, 32))
field_controller = pyogi.XBar(10, minimal=3, step=1, font=DEFAULTFONT)

def set_theme(text):
    new_theme = themes[text]
    if new_theme:
        config.config(theme=new_theme)
    else:
        config.remove_config("theme")

themen_box = pyogi.Listbox(DEFAULTFONT, (80, 60), event=set_theme)
themen_box.add_rows("Keines", "Dark", "Normal", "Crazy")
themen_frame = pyogi.Frame(themen_box)

normal = pyogi.Checkbutton(label=pyogi.Text("Normal", DEFAULTFONT),
                           size=(128, 32), event=lambda state: gui.config(
                               theme=DEFAULT_THEME))
dark = pyogi.Checkbutton(label=pyogi.Text("Dark", DEFAULTFONT), size=(128, 32),
                         event=lambda state: gui.config(theme=dark_theme))
crazy = pyogi.Checkbutton(label=pyogi.Text("Crazy", DEFAULTFONT),
                          size=(128, 32), event=lambda state: gui.config(
                              theme=crazy_theme))

normal.connect(dark)
normal.connect(crazy)

config._slave.place(field_controller, side="center")
gui.place(themen_wahl, side="midtop")

pyogi.place.to(themen_wahl, dark, side="midtop", offset=(0, 0))
pyogi.place.to(dark, crazy, side="midleft", offset=(0, 0))
pyogi.place.to(dark, normal, side="midright", offset=(0, 0))
pyogi.place.to(dark, config, side="midtop", offset=(0, 0))
pyogi.place.to(config, themen_frame, side="midtop", offset=(0, 0))

if __name__ == '__main__':

    while True:
        for event in pygame.event.get():
            gui.test_event(event)
            etype = event.type
            if etype == QUIT:
                sys.exit()
            elif etype == VIDEORESIZE:
                pass

        #muss immer aufgerufen werden
        gui.update()
        if gui.changed: #prueft auf eine AEnderung in der Gui
            display.fill((0, 0, 0))
            gui.draw(display) #Zeichnet die Gui auf display

        past_time = CLOCK.tick(FPS)
        pygame.display.flip()
