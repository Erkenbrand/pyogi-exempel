#!/usr/bin/env python3

"""3 Gewinnt Beispiel"""

import sys

import pygame
from pygame.freetype import SysFont

import pyogi
from pyogi.locals import *
import pyogi.theme.dark as dark_theme
import pyogi.theme.crazy as crazy_theme


#init der pygame Umgebung
pygame.init()
pygame.key.set_repeat(1, 1)

#Erstelle ein Standard Font Objekt fuer die Gui
#Es muss ein freetype Font sein
DEFAULTFONT = SysFont(None, 18)
RESULUTION = (600, 600)


class World:

    def __init__(self, scene):
        """
        Die  Wurzel des kompletten Programms,
        die immer die aktuell zugewiesene Szene darstellt.
        """

        self.clock = pygame.time.Clock()
        self.image = pygame.display.set_mode(RESULUTION, RESIZABLE)
        scene.master = self
        self.representation = scene

    def set_scene(self, scene):
        """Hier wird eine neue darzustellende Szene uebergeben"""

        scene.master = self
        self.image.fill((0, 0, 0))
        del self.representation.master
        self.representation = scene

    def input_handling(self):
        """Hier werden die Input Methoden der uebergebenen Szene aufgerufen"""

        for event in pygame.event.get():
            if event.type == VIDEORESIZE:
                self.image = pygame.display.set_mode(event.size, RESIZABLE)
            self.representation.event_handling(event)

    def main(self, fps=30):
        """Startet das Programm"""

        while self.representation:
            past_time = self.clock.tick(fps)
            self.input_handling()
            self.representation.update(past_time=past_time)
            self.representation.update_display(self.image)
            pygame.display.flip()


class Gamefield:

    def __init__(self, number_fields=3):
        """Verwaltet und ueberprüft die spiele Daten"""

        self.field = [["", ] * number_fields for i in range(number_fields)]
        self.number_fields = number_fields
        self.field_set = 0

    def set_side(self, pos=(0, 0), character=""):
        """
        Setzt ein Feld auf einen uebergebenen string.
        Ist eine reihe mit einem gleichen string voll,
        wird dieser zurueck gegeben.
        Ist an der uebergebenen stelle ein string, wird False zurück gegeben.
        Ist das Feld ohne Sieger voll, wird None zurückgegeben.
        """

        row = self.field[pos[1]]
        if row[pos[0]]:
            return False
        else:
            self.field_set += 1
            row[pos[0]] = character
            test_rows = set()
            test_rows.add("".join(row))
            row = []
            for i in self.field:
                row.append(i[pos[0]])
            test_rows.add("".join(row))
            row1 = []
            row2 = []
            for i in range(self.number_fields):
                row1.append(self.field[i][i])
                row2.append(self.field[i][self.number_fields - i - 1])
            test_rows |= set(("".join(row1), "".join(row2)))
            if self.test_rows(test_rows, character):
                return character
            if self.field_set == self.number_fields * self.number_fields:
                return None
        return True

    def test_rows(self, rows, character):
        """
        Ist eins der übergebenen listen gefüllt mit dem übergebenen string,
        wird True zurückgegebenen
        """

        must_be = character * self.number_fields
        for i in rows:
            if i == must_be:
                return True

    def get_pos(self, pos):
        """Gieb den string des feldes an pos zurueck."""

        return self.field[pos[1]][pos[0]]


class Menue:

    def __init__(self, default=3, default_theme=DEFAULT_THEME, size=RESULUTION):
        """Die Szene die das Menue des Programms repraesentiert"""

        self.game_theme = default_theme
        self.gui = pyogi.Gui(size, theme=default_theme)
        self.start = pyogi.Button(label=pyogi.Text("Start", DEFAULTFONT),
                                  event=self.start_game)
        self.exit = pyogi.Button(label=pyogi.Text("Exit", DEFAULTFONT),
                                 event=sys.exit)
        self.config_container = pyogi.Container((230, 42))
        self.config = pyogi.Frame(self.config_container)
        self.field_controller_label = pyogi.Label("felder anzahl", DEFAULTFONT,
                                          size=(120, 32))
        self.themen_wahl = pyogi.Label("themen wahl", DEFAULTFONT,
                                          size=(120, 32))
        self.field_controller = pyogi.XBar(10, minimal=3, step=1,
                                           font=DEFAULTFONT)
        if 10 >= default >= 3:
            self.field_controller.set(default)
        self.normal = pyogi.Checkbutton(
            label=pyogi.Text("Normal", DEFAULTFONT), size=(128, 32),
            event=lambda state: self.set_game_theme(DEFAULT_THEME))
        self.dark = pyogi.Checkbutton(
            label=pyogi.Text("Dark", DEFAULTFONT), size=(128, 32),
            event=lambda state: self.set_game_theme(dark_theme))
        self.crazy = pyogi.Checkbutton(
            label=pyogi.Text("Crazy", DEFAULTFONT),size=(128, 32),
            event=lambda state: self.set_game_theme(crazy_theme))
        self.normal.connect(self.dark)
        self.normal.connect(self.crazy)
        if default_theme == dark_theme:
            self.dark.switch()
        elif default_theme == crazy_theme:
            self.crazy.switch()
        self.config_container.place(self.field_controller_label, side="midleft")
        pyogi.place.to(self.field_controller_label, self.field_controller,
                       side="midleft", offset=(2, 0))

        self.gui.place(self.start, (0, -10), "center", link=True)
        pyogi.place.to(self.start, self.exit, "midtop", link=True)
        pyogi.place.to(self.start, self.config, "midbottom", offset=(0, -10),
                      link=True)
        self.gui.place(self.themen_wahl, side="midtop", link=True)
        pyogi.place.to(self.themen_wahl, self.dark, "midtop", link=True)
        pyogi.place.to(self.dark, self.normal, "midright", link=True)
        pyogi.place.to(self.dark, self.crazy, "midleft", link=True)

    def set_game_theme(self, value):
        """Legt das Thema des Spieles fest."""

        self.game_theme = value
        self.gui.config(theme=value)

    def start_game(self):
        """Erstellt eine Instanz der Game Klasse und setze den Master darauf."""
        self.master.set_scene(Game(self.field_controller.get(),
                                   self.game_theme,
                                   size=pygame.display.get_surface().get_size())
                              )

    def update(self, past_time):
        self.gui.update()

    def update_display(self, display):
        if self.gui.changed: #prueft auf eine AEnderung in der Gui
            self.gui.draw(display) #Zeichnet die Gui auf buffer

    def event_handling(self, event):
        self.gui.test_event(event)


class Game:

    def __init__(self, number_fields=3, theme=None, size=(600, 600)):
        """Verwaltet das gesamte Spiel."""

        self.number_fields = number_fields
        self.player = "X"
        self.gamerun = True
        self.gamefield = Gamefield(self.number_fields)
        self.gui = pyogi.Gui(size, theme=theme)
        self.graphic_field = pyogi.Gride()
        self.frame = pyogi.Frame(self.graphic_field, size="fill")
        for y in range(self.number_fields):
            for x in range(self.number_fields):
                label = pyogi.Text("", DEFAULTFONT)
                pos = (x, y)
                button = pyogi.Button(
                    label, (0, 0), event=lambda label=label, pos=pos: (
                        self.set_field(label, pos)))
                self.graphic_field.add(button)
        text = pyogi.Text("Neustart", DEFAULTFONT)
        self.restart = pyogi.Button(
            text, event=lambda: self.master.set_scene(
                Game(self.number_fields, self.gui._theme,
                     size=pygame.display.get_surface().get_size())))
        text = pyogi.Text("Zurück", DEFAULTFONT)
        self.back = pyogi.Button(
            text, event=lambda: self.master.set_scene(
                Menue(self.number_fields, self.gui._theme,
                      size=pygame.display.get_surface().get_size())))

        self.info = pyogi.Label("jetzt ist spieler X dran", DEFAULTFONT,
                                 size=(200, 20), widget_bg=(0, 0, 0),
                                 text_fg=(255, 100, 100))

        self.gui.place(self.restart, side="midtop", link=True)
        self.gui.place(self.back, side="midbottom", link=True)
        pyogi.place.to(self.restart, self.info, "midtop", link=True)
        self.gui.add(self.frame)
        self.frame.link_size(self.info, self.back, "midbottom", "midtop",
                             mode="h")

    def set_field(self, label, pos):
        """Setzt das event vuer einen Button des Spielfeldes"""
        if self.gamerun:
            event = self.gamefield.set_side(pos, self.player)
            if event == self.player:
                label.text = self.player
                self.info.text = "spieler %s gewinnt" % (self.player,)
                self.gamerun = False
            elif event is None:
                self.info.text = "nimand gewinnt"
                label.text = self.player
                self.gamerun = False
            elif event:
                label.text = self.player
                self.player = "X" if "X" != self.player else "O"
                self.info.text = "jetzt ist spieler %s dran" % (
                    self.player,)

    def update(self, past_time):
        self.gui.update()

    def update_display(self, display):
        if self.gui.changed: #prueft auf eine AEnderung in der Gui
            self.gui.draw(display) #Zeichnet die Gui auf buffer

    def event_handling(self, event):
        self.gui.test_event(event)

if __name__ == '__main__':
    #Ersellung der Umgebung
    game = World(Menue())
    game.main()
