#!/usr/bin/env python3

"""Savedialog Test und Beispiel"""

import sys
import pygame
from pygame.freetype import SysFont

import pyogi
from pyogi.locals import * # pygame.locals werden mit importiert


#init der pygame Umgebung
pygame.init()
pygame.key.set_repeat(1, 1) #sollte immer gesetzt werden

resulution = (600, 600)
display = pygame.display.set_mode(resulution, RESIZABLE)
CLOCK = pygame.time.Clock()
FPS = 30

#Erstelle ein Standard Font Objekt fuer die Gui.
#Es muss ein free4type Font sein.
DEFAULTFONT = SysFont(None, 12)
DEFAULTFONT.pad = True

DEFAULTFONTVERTIVAL = SysFont(None, 12)

#Das Haupt Fenster, der Gui mit der Groeße des ganzen Fensters.
gui = pyogi.Gui(resulution)

#Erstellung aller anderen Gui Elemente.

def save_file(path):
    print(path)

file = pyogi.dialog.Save(DEFAULTFONT, "fill", event=save_file,
                         killevent=sys.exit, file_filter=("all", ".py",
                                                          ".png .bmp", ".text"))

#Fuege file dem interface hinzu.
gui.add(file)

if __name__ == '__main__':

    while True:
        for event in pygame.event.get():
            gui.test_event(event)
            etype = event.type
            if etype == QUIT:
                sys.exit()
            elif etype == VIDEORESIZE:
                display = pygame.display.set_mode(event.size, RESIZABLE)

        #muss immer aufgerufen werden
        gui.update()
        if gui.changed: #prueft auf eine AEnderung in der Gui
            display.fill((0, 0, 0))
            gui.draw(display) #Zeichnet die Gui auf display

        past_time = CLOCK.tick(FPS)
        pygame.display.flip()
